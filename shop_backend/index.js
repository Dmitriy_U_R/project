require('dotenv').config()

const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const PORT = 3000
const URL = 'mongodb+srv://basicUser:159159@cluster0-zqzlu.mongodb.net/products'
const jwt = require('jsonwebtoken')

const app = express()

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, PATCH, PUT, POST, DELETE, OPTIONS");
    next();
});

app.use(bodyParser.json());
app.use(express.json());

async function start() {
    try {
        await mongoose.connect(URL, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        })
        app.listen(PORT, () => {
            console.log('Server has been started')
        })
    } catch (err) {
        console.error(err)
    }
}


const productsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    image: String,
    Name: String,
    id: Number,
});

const UsersSchema = new mongoose.Schema({
    email: String,
    password: String,
    id: Number,
});


const Products = mongoose.model('Products', productsSchema);

const User = mongoose.model('Users', UsersSchema);

app.get('/api/v1/products', async (req, res) => {
    await Products.find({}).exec(function (error, data) {
        if (!error) {
            console.log(data)
            res.json({ data })
        } else {
            console.log("error: ", error)
            return res.status(408).send({
                message: error
            });
        }
    });
})

app.get('/api/v1/product/:id', authenticateToken, async (req, res) => {
    console.log(req.headers['authorization'])
    await Products.find({ id: { $gt: +req.params.id, $lt: +req.params.id + 5 } }).exec(function (error, data) {
        if (!error) {
            console.log(req.params.id, data)
            res.json({ data })
        } else {
            console.log("error: ", error)
            return res.status(408).send({
                message: error
            });
        }
    });
})

app.get('/api/v1/products/:name', authenticateToken, async (req, res) => {
    console.log(req.headers['authorization'])
    let searchName = req.params.name.toLowerCase()
    console.log(searchName)
    await Products.find({ $or: [{ normolizeName: { $regex: searchName } }, { description: { $regex: req.params.name } }] }).exec(function (error, data) {
        if (!error) {
            console.log(req.params.name, data)
            res.json({ data })
        } else {
            console.log("error: ", error)
            return res.status(408).send({
                message: error
            });
        }
    });
})

app.post(`/api/v1/users/delete`, async (req, res) => {
    User.deleteOne({ $and: [{ email: req.body.email }, { password: req.body.password }] }).exec(function (error, data) {
        if (!error) {
            console.log("This user has been sucsessfully deleted:", req.body.email, req.body.password)
            console.log(data)
            res.json('User has been successfully deleted.')
        } else {
            console.log(error)
            return res.status(408).send({
                message: "error: No such user", error
            });
        }
    })
})

app.post(`/api/v1/users`, async (req, res) => {
    const body = new User({
        email: req.body.email,
        password: req.body.password,
    })
    User.find({ email: req.body.email }).exec(function (error, data) {
        if (data.length == 0) {
            console.log("data", data)
            body.save(function (err) {
                res.json('User has been successfully saved.')
                console.log('User has been successfully saved.')
            })
        } else {
            console.log("error: User with such email is already exist", error)
            return res.status(408).send({
                message: "error: User with such email is already exist", error
            });
        }
    })
})

app.post('/api/v1/check', async (req, res) => {
    console.log(req.body)
    User.find({ email: req.body.email }).where({ password: req.body.password }).exec(function (error, data) {
        if (data.length > 0) {
            console.log("data", data)
            const user = { email: req.body.email }
            const accessToken = generateAccessToken(user)
            res.status(200).send({ token: accessToken })
        }
        else {
            console.log("Cant find such user", error)
            return res.status(408).send({
                error
            })
        }
    })
})


function generateAccessToken(user) {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '600s' })
}

function authenticateToken(req, res, next) {
    //const authHeader = req.headers['authorization']
    console.log(req.headers['authorization'])
    next()
    /*const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        console.log(err)
        if (err) return res.sendStatus(403)
        req.user = user
        next()
    })*/
}

start()